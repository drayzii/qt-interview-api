# Task Manager
Task Manager API description

## Steps to run

- Clone the repo
- Add `.env` file with this template
```
DATABASE_URL=
JWT_SECRET=
```
- Run `yarn && yarn prisma migrate dev`. This will install packages and run DB migrations and seed data.
- Run `yarn start:dev`
- The app should be running on port `3000`

## Documentation

The Swagger documentation can be found on `http://localhost:3000/api`
