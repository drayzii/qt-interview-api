-- CreateTable
CREATE TABLE "taskProjects" (
    "id" TEXT NOT NULL,
    "taskId" TEXT NOT NULL,
    "projectId" TEXT NOT NULL,

    CONSTRAINT "taskProjects_pkey" PRIMARY KEY ("id")
);

-- AddForeignKey
ALTER TABLE "taskProjects" ADD CONSTRAINT "taskProjects_taskId_fkey" FOREIGN KEY ("taskId") REFERENCES "tasks"("id") ON DELETE RESTRICT ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "taskProjects" ADD CONSTRAINT "taskProjects_projectId_fkey" FOREIGN KEY ("projectId") REFERENCES "projects"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
