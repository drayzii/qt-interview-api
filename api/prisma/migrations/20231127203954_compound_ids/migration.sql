/*
  Warnings:

  - The primary key for the `taskAssignees` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `taskAssignees` table. All the data in the column will be lost.
  - The primary key for the `taskProjects` table will be changed. If it partially fails, the table could be left without primary key constraint.
  - You are about to drop the column `id` on the `taskProjects` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE "taskAssignees" DROP CONSTRAINT "taskAssignees_pkey",
DROP COLUMN "id",
ADD CONSTRAINT "taskAssignees_pkey" PRIMARY KEY ("taskId", "userId");

-- AlterTable
ALTER TABLE "taskProjects" DROP CONSTRAINT "taskProjects_pkey",
DROP COLUMN "id",
ADD CONSTRAINT "taskProjects_pkey" PRIMARY KEY ("taskId", "projectId");
