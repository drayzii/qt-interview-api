-- DropForeignKey
ALTER TABLE "taskAssignees" DROP CONSTRAINT "taskAssignees_taskId_fkey";

-- DropForeignKey
ALTER TABLE "taskProjects" DROP CONSTRAINT "taskProjects_taskId_fkey";

-- AddForeignKey
ALTER TABLE "taskAssignees" ADD CONSTRAINT "taskAssignees_taskId_fkey" FOREIGN KEY ("taskId") REFERENCES "tasks"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "taskProjects" ADD CONSTRAINT "taskProjects_taskId_fkey" FOREIGN KEY ("taskId") REFERENCES "tasks"("id") ON DELETE CASCADE ON UPDATE CASCADE;
