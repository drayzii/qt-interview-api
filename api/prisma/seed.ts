import { PrismaClient } from '@prisma/client';

const prisma = new PrismaClient();

async function main() {
  const projects = await prisma.projects.createMany({
    data: [
      {
        name: 'Hotel Management System'
      },
      {
        name: 'Task Management Dashboard'
      }
    ]
  });

  console.log(projects);
}

main()
  .then(async () => {
    await prisma.$disconnect();
  })
  .catch(async (e) => {
    console.error(e);
    await prisma.$disconnect();
    process.exit(1);
  });
