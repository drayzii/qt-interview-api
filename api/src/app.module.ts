import { Module } from '@nestjs/common';
import { RouterModule } from '@nestjs/core';
import { ConfigModule } from '@nestjs/config';
import { AuthModule } from './auth/auth.module';
import { PrismaModule } from './prisma/prisma.module';
import { UsersModule } from './users/users.module';
import { ProjectsModule } from './projects/projects.module';
import { TasksModule } from './tasks/tasks.module';

@Module({
  imports: [
    ConfigModule.forRoot({ isGlobal: true }),
    PrismaModule,
    RouterModule.register([
      {
        path: 'auth',
        module: AuthModule
      },
      {
        path: 'users',
        module: UsersModule
      },
      {
        path: 'projects',
        module: ProjectsModule
      },
      {
        path: 'tasks',
        module: TasksModule
      }
    ]),
    AuthModule,
    UsersModule,
    ProjectsModule,
    TasksModule
  ]
})
export class AppModule {}
