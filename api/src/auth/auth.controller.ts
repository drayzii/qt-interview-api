import {
  Body,
  Controller,
  Patch,
  Post,
  UseGuards,
  Query
} from '@nestjs/common';
import { AuthService } from './auth.service';
import {
  RegisterDto,
  LoginDto,
  ChangePasswordDto,
  ResetPasswordRequestDto,
  ResetPasswordQuery,
  ResetPasswordDto
} from './dto';
import { AuthGuard } from './auth.guard';
import { RequestUser, RequestUserDTO } from './auth.decorator';

@Controller()
export class AuthController {
  constructor(private readonly authService: AuthService) {}

  @Post('/register')
  register(@Body() dto: RegisterDto) {
    return this.authService.createUser(dto);
  }

  @Post('/login')
  login(@Body() dto: LoginDto) {
    return this.authService.login(dto);
  }

  @UseGuards(AuthGuard)
  @Patch('/change-password')
  changePassword(
    @RequestUser() user: RequestUserDTO,
    @Body() dto: ChangePasswordDto
  ) {
    return this.authService.changePassword(user.id, dto);
  }

  @Patch('/reset-password-request')
  resetPassword(@Body() dto: ResetPasswordRequestDto) {
    return this.authService.resetPasswordRequest(dto.email);
  }

  @Patch('/reset-password')
  newPassword(
    @Query() queryParams: ResetPasswordQuery,
    @Body() dto: ResetPasswordDto
  ) {
    return this.authService.resetPassword(queryParams.token, dto.password);
  }
}
