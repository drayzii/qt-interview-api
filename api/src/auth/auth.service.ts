import * as bcrypt from 'bcrypt';
import {
  Injectable,
  ForbiddenException,
  ConflictException
} from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { PrismaService } from '../prisma/prisma.service';
import { RegisterDto, LoginDto } from './dto';

@Injectable()
export class AuthService {
  constructor(
    private prisma: PrismaService,
    private jwt: JwtService,
    private config: ConfigService
  ) {}

  async createUser(dto: RegisterDto) {
    const existingUser = await this.prisma.users.findUnique({
      where: {
        email: dto.email
      }
    });

    if (existingUser)
      throw new ConflictException('User with the same email already exists');

    const hashedPassword = await bcrypt.hash(dto.password, 10);

    const user = await this.prisma.users.create({
      data: {
        email: dto.email,
        firstName: dto.firstName,
        lastName: dto.lastName,
        password: hashedPassword
      }
    });

    delete user.password;

    return { user };
  }

  async login(dto: LoginDto) {
    const user = await this.prisma.users.findUnique({
      where: {
        email: dto.email
      }
    });

    if (!user) throw new ForbiddenException('Credentials Incorrect');

    const passwordMatches = await bcrypt.compare(dto.password, user.password);
    if (!passwordMatches) throw new ForbiddenException('Credentials Incorrect');

    return {
      accessToken: await this.signToken({ id: user.id }),
      user: {
        firstName: user.firstName,
        lastName: user.lastName,
        email: user.email,
        picture: user.picture
      }
    };
  }

  private async signToken(payload): Promise<string> {
    const secret = this.config.get('JWT_SECRET');

    const token = await this.jwt.signAsync(payload, {
      expiresIn: '1h',
      secret: secret
    });

    return token;
  }

  async changePassword(id, dto) {
    const user = await this.prisma.users.findUnique({ where: { id } });
    if (!user) throw new ForbiddenException('Invalid user id');

    const passwordMatches = await bcrypt.compare(
      dto.oldPassword,
      user.password
    );
    if (!passwordMatches) {
      throw new ForbiddenException('Old password is incorrect');
    }

    const hashedPassword = await bcrypt.hash(dto.password, 10);

    const response = await this.prisma.users.update({
      where: { id },
      data: { password: hashedPassword },
      select: { id: true }
    });

    return { success: !!response };
  }

  async resetPasswordRequest(email) {
    const user = await this.prisma.users.findUnique({
      where: { email }
    });

    if (!user) throw new ForbiddenException('Email does not exist');

    const resetToken = await this.prisma.passwordResetTokens.create({
      data: {
        userId: user.id,
        token: await this.signToken({ id: user.id })
      }
    });

    return resetToken.token;
  }

  async resetPassword(token, password) {
    const payload = await this.jwt.verifyAsync(token, {
      secret: this.config.get('JWT_SECRET')
    });

    if (!payload) throw new ForbiddenException('Invalid password reset token');

    const resetToken = await this.prisma.passwordResetTokens.findFirst({
      where: { token }
    });

    const hashedPassword = await bcrypt.hash(password, 10);

    const updatedUser = await this.prisma.users.update({
      where: { id: resetToken.userId },
      data: { password: hashedPassword },
      select: { id: true }
    });

    const response = await this.prisma.passwordResetTokens.deleteMany({
      where: {
        userId: updatedUser.id
      }
    });

    return { success: !!response };
  }
}
