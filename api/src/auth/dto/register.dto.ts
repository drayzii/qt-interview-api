import {
  IsEmail,
  IsNotEmpty,
  IsString,
  MinLength,
  IsUrl,
  IsOptional
} from 'class-validator';

export class RegisterDto {
  @IsEmail({}, { message: 'Please provide a valid email address' })
  @IsNotEmpty()
  email: string;

  @IsString()
  @IsNotEmpty({ message: 'Please add your first name' })
  firstName: string;

  @IsString()
  @IsNotEmpty({ message: 'Please add your last name' })
  lastName: string;

  @IsOptional()
  @IsString()
  @IsUrl({}, { message: 'Please add a valid picture URL' })
  picture: string;

  @IsString()
  @MinLength(10)
  password: string;
}
