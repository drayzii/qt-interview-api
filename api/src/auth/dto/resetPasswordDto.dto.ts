import { IsString, MinLength } from 'class-validator';

export class ResetPasswordQuery {
  @IsString()
  token: string;
}

export class ResetPasswordDto {
  @IsString()
  @MinLength(10)
  password: string;
}
