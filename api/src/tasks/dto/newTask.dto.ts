import {
  IsNotEmpty,
  IsString,
  MaxLength,
  IsUrl,
  IsOptional,
  IsDateString,
  IsUUID,
  IsEnum
} from 'class-validator';

export type Priority = 'low' | 'normal' | 'high';
export const Priorities = ['low', 'normal', 'high'];

export class NewTaskDto {
  @IsString()
  @IsNotEmpty({ message: 'Project name is required' })
  name: string;

  @IsString()
  @MaxLength(100, {
    message: 'Project Description must be 100 character at most'
  })
  @IsNotEmpty({ message: 'Project description is required' })
  description: string;

  @IsString()
  @IsNotEmpty({ message: 'Project priority is required' })
  @IsEnum(Priorities, { message: 'Priority can only be low, normal or high' })
  priority: string;

  @IsOptional()
  @IsString()
  @IsUrl({}, { message: 'Please add a valid attachment URL' })
  attachment: string;

  @IsDateString()
  @IsNotEmpty()
  startAt: Date;

  @IsDateString()
  @IsNotEmpty()
  endAt: Date;

  @IsUUID(4, { each: true })
  @IsNotEmpty({ each: true })
  assignees: string[];

  @IsUUID(4, { each: true })
  @IsNotEmpty({ each: true })
  projects: string[];
}
