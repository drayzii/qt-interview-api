import { IsOptional, IsString } from 'class-validator';

export class TaskFiltersDto {
  @IsString()
  page: string;

  @IsString()
  pageSize: string;

  @IsOptional()
  @IsString()
  priority: string;

  @IsOptional()
  @IsString()
  name: string;

  @IsOptional()
  @IsString()
  query: string;
}
