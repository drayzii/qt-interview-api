import {
  IsNotEmpty,
  IsString,
  MaxLength,
  IsUrl,
  IsOptional,
  IsDateString,
  IsUUID,
  IsEnum
} from 'class-validator';

export type PriorityUpdate = 'low' | 'normal' | 'high';
export const PrioritiesUpdate = ['low', 'normal', 'high'];

export class UpdateTaskDto {
  @IsOptional()
  @IsString()
  @IsNotEmpty({ message: 'Project name is required' })
  name: string;

  @IsOptional()
  @IsString()
  @MaxLength(100, {
    message: 'Project Description must be 100 character at most'
  })
  @IsNotEmpty({ message: 'Project description is required' })
  description: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty({ message: 'Project priority is required' })
  @IsEnum(PrioritiesUpdate, {
    message: 'Priority can only be low, normal or high'
  })
  priority: string;

  @IsOptional()
  @IsString()
  @IsUrl({}, { message: 'Please add a valid attachment URL' })
  attachment: string;

  @IsOptional()
  @IsDateString()
  @IsNotEmpty()
  startAt: Date;

  @IsOptional()
  @IsDateString()
  @IsNotEmpty()
  endAt: Date;

  @IsOptional()
  @IsUUID(4, { each: true })
  @IsNotEmpty({ each: true })
  assignees: string[];

  @IsOptional()
  @IsUUID(4, { each: true })
  @IsNotEmpty({ each: true })
  projects: string[];
}
