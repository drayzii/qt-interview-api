import {
  Controller,
  Get,
  Post,
  UseGuards,
  Body,
  ParseUUIDPipe,
  Param,
  Patch,
  Query,
  Delete
} from '@nestjs/common';
import { TasksService } from './tasks.service';
import { AuthGuard } from '../auth/auth.guard';
import { RequestUser, RequestUserDTO } from '../auth/auth.decorator';
import { NewTaskDto, UpdateTaskDto, TaskFiltersDto } from './dto';

@UseGuards(AuthGuard)
@Controller()
export class TasksController {
  constructor(private readonly tasksService: TasksService) {}

  @Get('/')
  getTasks() {
    return this.tasksService.getTasks();
  }

  @Get('/filter')
  getTasksWithFilters(@Query() queryParams: TaskFiltersDto) {
    return this.tasksService.getTasksWithFilters(queryParams);
  }

  @Post('/')
  createTask(@RequestUser() user: RequestUserDTO, @Body() dto: NewTaskDto) {
    return this.tasksService.addTask(dto, user);
  }

  @Get('/:id')
  getTask(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.tasksService.getTask(id);
  }

  @Patch('/:id')
  updateTask(
    @RequestUser() user: RequestUserDTO,
    @Param('id', new ParseUUIDPipe()) id: string,
    @Body() dto: UpdateTaskDto
  ) {
    return this.tasksService.updateTask(id, dto, user);
  }

  @Delete('/:id')
  deleteTask(
    @RequestUser() user: RequestUserDTO,
    @Param('id', new ParseUUIDPipe()) id: string
  ) {
    return this.tasksService.deleteTask(id, user);
  }
}
