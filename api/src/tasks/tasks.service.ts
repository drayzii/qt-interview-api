import {
  Injectable,
  NotFoundException,
  ForbiddenException
} from '@nestjs/common';
import { PrismaService } from '../prisma/prisma.service';

@Injectable()
export class TasksService {
  constructor(private prisma: PrismaService) {}

  async getTasks() {
    const tasks = await this.prisma.tasks.findMany({
      select: {
        id: true,
        name: true,
        description: true,
        priority: true,
        startAt: true,
        endAt: true,
        createdBy: {
          select: {
            id: true,
            email: true,
            firstName: true,
            lastName: true,
            picture: true
          }
        },
        assignees: {
          select: {
            user: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true,
                picture: true
              }
            }
          }
        },
        projects: {
          select: {
            project: true
          }
        }
      }
    });
    return tasks;
  }

  async getTasksWithFilters(queryParams) {
    const page = parseInt(queryParams.page, 10);
    const pageSize = parseInt(queryParams.pageSize, 10);

    const take = pageSize > 0 ? pageSize : 10;
    const skip = (page - 1) * take;

    const query = {
      take,
      skip,
      where: {
        priority: queryParams.priority,
        name: {
          contains: queryParams.name
        }
      },
      select: {
        id: true,
        name: true,
        description: true,
        priority: true,
        startAt: true,
        endAt: true,
        createdBy: {
          select: {
            id: true,
            email: true,
            firstName: true,
            lastName: true,
            picture: true
          }
        },
        assignees: {
          select: {
            user: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true,
                picture: true
              }
            }
          }
        },
        projects: {
          select: {
            project: true
          }
        }
      }
    };

    const [tasks, count] = await this.prisma.$transaction([
      this.prisma.tasks.findMany(query),
      this.prisma.tasks.count({ where: query.where })
    ]);

    return {
      total: count,
      tasks
    };
  }

  async addTask(dto, user) {
    const data = {
      ...dto,
      assignees: {
        create: dto.assignees.map((assignee) => ({ userId: assignee }))
      },
      projects: {
        create: dto.projects.map((project) => ({ projectId: project }))
      },
      createdById: user.id
    };

    const task = await this.prisma.tasks.create({
      data,
      select: {
        id: true,
        name: true,
        description: true,
        priority: true,
        startAt: true,
        endAt: true,
        createdBy: {
          select: {
            id: true,
            email: true,
            firstName: true,
            lastName: true,
            picture: true
          }
        },
        assignees: {
          select: {
            user: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true,
                picture: true
              }
            }
          }
        },
        projects: {
          select: {
            project: true
          }
        }
      }
    });
    return task;
  }

  async getTask(id) {
    const task = await this.prisma.tasks.findUnique({
      where: { id },
      select: {
        id: true,
        name: true,
        description: true,
        priority: true,
        startAt: true,
        endAt: true,
        createdBy: {
          select: {
            id: true,
            email: true,
            firstName: true,
            lastName: true,
            picture: true
          }
        },
        assignees: {
          select: {
            user: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true,
                picture: true
              }
            }
          }
        },
        projects: {
          select: {
            project: true
          }
        }
      }
    });

    if (!task) throw new NotFoundException('Invalid task id');

    return task;
  }

  private formatTaskUpdateData(task, dto) {
    const { assignees, projects, ...update } = dto;

    if (assignees?.length) {
      const assigneesUpdate = {
        upsert: assignees.map((userId) => ({
          where: {
            taskId_userId: {
              taskId: task.id,
              userId
            }
          },
          create: { userId },
          update: { userId }
        })),
        deleteMany: {}
      };

      assigneesUpdate.deleteMany = {
        taskId: task.id,
        userId: { notIn: assignees }
      };

      update.assignees = assigneesUpdate;
    }

    if (projects?.length) {
      const projectsUpdate = {
        upsert: projects.map((projectId) => ({
          where: {
            taskId_projectId: {
              taskId: task.id,
              projectId
            }
          },
          create: { projectId },
          update: { projectId }
        })),
        deleteMany: {}
      };

      projectsUpdate.deleteMany = {
        taskId: task.id,
        projectId: { notIn: projects }
      };

      update.projects = projectsUpdate;
    }

    return update;
  }

  async updateTask(id, dto, user) {
    const task = await this.prisma.tasks.findUnique({ where: { id } });

    if (!task) throw new NotFoundException('Invalid task id');
    if (task.createdById !== user.id) {
      throw new ForbiddenException('You cannot edit this task');
    }

    const response = await this.prisma.tasks.update({
      where: { id },
      data: this.formatTaskUpdateData(task, dto),
      select: {
        id: true,
        name: true,
        description: true,
        priority: true,
        startAt: true,
        endAt: true,
        createdBy: {
          select: {
            id: true,
            email: true,
            firstName: true,
            lastName: true,
            picture: true
          }
        },
        assignees: {
          select: {
            user: {
              select: {
                id: true,
                email: true,
                firstName: true,
                lastName: true,
                picture: true
              }
            }
          }
        },
        projects: {
          select: {
            project: true
          }
        }
      }
    });

    return response;
  }

  async deleteTask(id, user) {
    const task = await this.prisma.tasks.findUnique({ where: { id } });

    if (!task) throw new NotFoundException('Invalid task id');
    if (task.createdById !== user.id) {
      throw new ForbiddenException('You cannot delete this task');
    }

    const response = await this.prisma.tasks.delete({
      where: { id }
    });

    return response;
  }
}
