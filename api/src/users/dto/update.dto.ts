import { IsNotEmpty, IsString, IsUrl, IsOptional } from 'class-validator';

export class UpdateDto {
  @IsOptional()
  @IsString()
  @IsNotEmpty({ message: 'Please add your first name' })
  firstName: string;

  @IsOptional()
  @IsString()
  @IsNotEmpty({ message: 'Please add your last name' })
  lastName: string;

  @IsOptional()
  @IsString()
  @IsUrl({}, { message: 'Please add a valid picture URL' })
  picture: string;
}
