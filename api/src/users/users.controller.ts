import {
  Controller,
  Get,
  UseGuards,
  ParseUUIDPipe,
  Param,
  Patch,
  Body
} from '@nestjs/common';
import { UsersService } from './users.service';
import { AuthGuard } from '../auth/auth.guard';
import { RequestUser, RequestUserDTO } from '../auth/auth.decorator';
import { UpdateDto } from './dto';

@UseGuards(AuthGuard)
@Controller()
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Get('/')
  getUsers() {
    return this.usersService.getUsers();
  }

  @Get('/me')
  getMyProfile(@RequestUser() user: RequestUserDTO) {
    return this.usersService.getProfile(user.id);
  }

  @Patch('/me')
  updateMyProfile(@RequestUser() user: RequestUserDTO, @Body() dto: UpdateDto) {
    return this.usersService.updateProfile(user.id, dto);
  }

  @Get('/:id')
  getUserProfile(@Param('id', new ParseUUIDPipe()) id: string) {
    return this.usersService.getProfile(id);
  }
}
