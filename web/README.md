# Web

Task Manager Frontend

## Steps to Run

- Run `yarn && yarn dev`
- The app should run on port `5173`

## Implemented functionality

### Authentication

- Users are able to create accounts with their names, email and password. Validations were added to prevent data errors
- Users can log in with their email and password
- Users can log out.

### Tasks Management

- Users can view all tasks, and filter them by name or category. The results are also paginated.
- Users can create tasks, assign projects and assignes, and add the timeline. Assignees and Projects can be edited during the task creation process.
- The task creator can edit all details related to the tasks
- The task creator can delete tasks

### Profile management

- User can update their names
- Users can update their passwords