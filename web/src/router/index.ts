import { createRouter, createWebHistory } from 'vue-router';
import Signup from '@/views/SignupScreen.vue';
import LoginScreen from '@/views/LoginScreen.vue';
import ForgotPassword from '@/views/ForgotPasswordScreen.vue';
import TasksScreen from '@/views/TasksScreen.vue';
import NewTaskScreen from '@/views/NewTaskScreen.vue';
import UpdateTaskScreen from '@/views/UpdateTaskScreen.vue';
import ProfileScreen from '@/views/ProfileScreen.vue';

const router = createRouter({
  history: createWebHistory(import.meta.env.BASE_URL),
  routes: [
    {
      path: '/signup',
      name: 'Sign up',
      component: Signup
    },
    {
      path: '/login',
      name: 'Log in',
      component: LoginScreen
    },
    {
      path: '/forgot-password',
      name: 'Forgot Password',
      component: ForgotPassword
    },
    {
      path: '/tasks',
      name: 'Tasks',
      component: TasksScreen
    },
    {
      path: '/new-task',
      name: 'New Task',
      component: NewTaskScreen
    },
    {
      path: '/update-task/:id',
      name: 'Update Task',
      component: UpdateTaskScreen
    },
    {
      path: '/my-profile',
      name: 'My Profile',
      component: ProfileScreen
    }
  ]
})

router.beforeEach((to, from, next) => {
  document.title = to.name as string;
  next();
});

export default router
