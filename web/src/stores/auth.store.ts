import router from '@/router';
import { defineStore } from 'pinia';

export const useAuthStore = defineStore({
  id: 'auth',
  state: () => ({
    accessToken: JSON.parse(localStorage.getItem('token') as string),
    user: JSON.parse(localStorage.getItem('user') as any),
  }),
  actions: {
    async login({ accessToken, user }: any) {
      this.user = user;
      localStorage.setItem('user', JSON.stringify(user));

      this.accessToken = accessToken;
      localStorage.setItem('accessToken', accessToken);
    },
    logout() {
      this.accessToken = null;
      localStorage.removeItem('accessToken');

      this.user = null;
      localStorage.removeItem('user');

      router.push('/login');
    }
  },
  getters: {
    userToken: (state) => {
      return state.accessToken;
    }
  }
});
