<template>
  <section class="bg-gray-50">
    <div class="flex flex-col items-center justify-center px-6 py-8 mx-auto md:h-screen lg:py-0">
        <a href="#" class="flex items-center mb-6 text-2xl font-semibold text-gray-900">
          Task Manager    
        </a>
        <div class="w-full bg-white rounded-lg shadow md:mt-0 sm:max-w-md xl:p-0">
          <div class="p-6 space-y-4 md:space-y-6 sm:p-8">
            <h1 class="text-xl font-bold leading-tight tracking-tight text-gray-900 md:text-2xl">
              Log in
            </h1>
            <form class="space-y-4 md:space-y-6" action="#">
              <div>
                <label for="email" class="block mb-2 text-sm font-medium text-gray-900">Your email</label>
                <input
                  type="email"
                  name="email"
                  id="email"
                  class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:border-primary-600 block w-full p-2.5"
                  placeholder="example@email.com"
                  v-model="state.email"
                  :class="{ 'border-red': !!v$.email.$error }"
                />
                <span v-if="v$.email.$error" class="text-red p-1"> {{ v$.email.$errors[0].$message }} </span>
              </div>
              <div>
                <label for="password" class="block mb-2 text-sm font-medium text-gray-900">Password</label>
                <input
                  type="password"
                  name="password"
                  id="password"
                  class="bg-gray-50 border border-gray-300 text-gray-900 sm:text-sm rounded-lg focus:border-primary-600 block w-full p-2.5"
                  placeholder="••••••••"
                  v-model="state.password"
                  :class="{ 'border-red': !!v$.password.$error }"
                />
                <span v-if="v$.password.$error" class="text-red p-1"> {{ v$.password.$errors[0].$message }} </span>
              </div>
              <button
                type="submit"
                class="w-full text-white bg-primary hover:bg-primary-700 focus:ring-4 focus:outline-none font-medium rounded-lg text-sm px-5 py-2.5 text-center"
                :disabled="!state.email || !state.password"
                @click.prevent="submit"
              >
                Log in
              </button>
              <span v-if="logInError" class="text-red p-1 mt-5">{{ logInError }}</span>
              <p class="text-sm font-light text-gray-500">
                New to Task Manager? <RouterLink to="/signup" class="font-medium text-primary-600 hover:underline">Create an account here</RouterLink>
              </p>
            </form>
          </div>
        </div>
    </div>
  </section>
</template>

<script setup lang="ts">
import { ref, reactive, computed } from 'vue';
import { RouterLink } from 'vue-router';
import useValidate from '@vuelidate/core';
import { required, email } from '@vuelidate/validators';
import axios from 'axios'
import router from '@/router';
import { useAuthStore } from '@/stores';

const state = reactive({
  email: '',
  password: ''
})

const logInError = ref('');

const rules = computed(() => ({
  email: { required, email },
  password: { required }
}));

const v$ = useValidate(rules, state);

const submit = () => {
  const result = v$.value.$validate();
  result.then((res) => {
    if(res) {
      return logIn(state)
    }
  }).catch((err) => {
    console.log(err);
  })
};

const logIn = async (credentials: any) => {
  try {
    logInError.value = '';
    const response = await axios({
      method: 'post',
      url: 'http://localhost:3000/auth/login',
      data: credentials
    });

    if (response.status === 201) {
      const authStore = useAuthStore();
      await authStore.login(response.data);

      alert('Logged in successfully');
      router.push('/tasks');
    } else {
      logInError.value = 'Log in failed'
    }
  } catch(err: any) {
    logInError.value =
      typeof err.response.data.message === 'string'
        ? err.response.data.message
        : err.response.data.message[0];
  }
};

</script>

<style>
</style>
