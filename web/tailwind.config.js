/* eslint-disable no-undef */
/** @type {import('tailwindcss').Config} */
module.exports = {
  content: [],
  purge: ['./index.html', './src/**/*.{vue,js,ts,jsx,tsx}'],
  theme: {
    extend: {},
    colors: {
      'white': '#ffffff',
      'primary': '#4083a1',
      'red': '#FF0000',
      'yellow': 'FFFF00',
      'orange': '#FFA500',
      'gray': '#333333',
      'gray-light': '#eeeeee',
      'green': '#008000'
    }
  },
  plugins: [],
}

